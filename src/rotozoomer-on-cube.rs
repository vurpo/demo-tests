#[macro_use]
extern crate glium;
extern crate image;
extern crate time;
extern crate cgmath;

#[derive(Copy, Clone)]
struct Vertex {
  position: [f32; 3],
  tex_coords: [f32; 2],
}

implement_vertex!(Vertex, position, tex_coords);

fn view_matrix(position: &[f32; 3], direction: &[f32; 3], up: &[f32; 3]) -> [[f32; 4]; 4] {
  let f = {
    let f = direction;
    let len = f[0] * f[0] + f[1] * f[1] + f[2] * f[2];
    let len = len.sqrt();
    [f[0] / len, f[1] / len, f[2] / len]
  };

  let s = [up[1] * f[2] - up[2] * f[1],
    up[2] * f[0] - up[0] * f[2],
    up[0] * f[1] - up[1] * f[0]];

  let s_norm = {
    let len = s[0] * s[0] + s[1] * s[1] + s[2] * s[2];
    let len = len.sqrt();
    [s[0] / len, s[1] / len, s[2] / len]
  };

  let u = [f[1] * s_norm[2] - f[2] * s_norm[1],
    f[2] * s_norm[0] - f[0] * s_norm[2],
    f[0] * s_norm[1] - f[1] * s_norm[0]];

  let p = [-position[0] * s_norm[0] - position[1] * s_norm[1] - position[2] * s_norm[2],
    -position[0] * u[0] - position[1] * u[1] - position[2] * u[2],
    -position[0] * f[0] - position[1] * f[1] - position[2] * f[2]];

  [
    [s[0], u[0], f[0], 0.0],
    [s[1], u[1], f[1], 0.0],
    [s[2], u[2], f[2], 0.0],
    [p[0], p[1], p[2], 1.0],
  ]
}

fn main() {
  use glium::{
    DisplayBuild,
    Surface,
  };

  let beginning_time = time::PreciseTime::now();

  let display = glium::glutin::WindowBuilder::new().with_dimensions(640, 480).with_depth_buffer(24).build_glium().unwrap();
  
  let hacklab_image = {
    use std::io::Cursor;

    let hacklab_texture = image::load(Cursor::new(&include_bytes!("../assets/hacklab.png")[..]), image::PNG).unwrap().to_rgba();
    let image_dimensions = hacklab_texture.dimensions();
    glium::texture::RawImage2d::from_raw_rgba_reversed(hacklab_texture.into_raw(), image_dimensions)
  };
  let hacklab_texture = glium::texture::SrgbTexture2d::new(&display, hacklab_image).unwrap();

  let shape = vec![
    //TOP FACE
    Vertex { position: [-1.0,-1.0, 1.0], tex_coords: [0.0, 0.0] },
    Vertex { position: [ 1.0,-1.0, 1.0], tex_coords: [1.0, 0.0] },
    Vertex { position: [ 1.0, 1.0, 1.0], tex_coords: [1.0, 1.0] },
    Vertex { position: [-1.0, 1.0, 1.0], tex_coords: [0.0, 1.0] },
    //BOTTOM FACE
    Vertex { position: [-1.0,-1.0,-1.0], tex_coords: [0.0, 0.0] },
    Vertex { position: [ 1.0,-1.0,-1.0], tex_coords: [1.0, 0.0] },
    Vertex { position: [ 1.0, 1.0,-1.0], tex_coords: [1.0, 1.0] },
    Vertex { position: [-1.0, 1.0,-1.0], tex_coords: [0.0, 1.0] },
    //LEFT FACE
    Vertex { position: [-1.0, 1.0,-1.0], tex_coords: [0.0, 0.0] },
    Vertex { position: [-1.0,-1.0,-1.0], tex_coords: [1.0, 0.0] },
    Vertex { position: [-1.0,-1.0, 1.0], tex_coords: [1.0, 1.0] },
    Vertex { position: [-1.0, 1.0, 1.0], tex_coords: [0.0, 1.0] },
    //RIGHT FACE
    Vertex { position: [ 1.0, 1.0,-1.0], tex_coords: [0.0, 0.0] },
    Vertex { position: [ 1.0,-1.0,-1.0], tex_coords: [1.0, 0.0] },
    Vertex { position: [ 1.0,-1.0, 1.0], tex_coords: [1.0, 1.0] },
    Vertex { position: [ 1.0, 1.0, 1.0], tex_coords: [0.0, 1.0] },
    //FRONT FACE
    Vertex { position: [-1.0,-1.0,-1.0], tex_coords: [0.0, 0.0] },
    Vertex { position: [ 1.0,-1.0,-1.0], tex_coords: [1.0, 0.0] },
    Vertex { position: [ 1.0,-1.0, 1.0], tex_coords: [1.0, 1.0] },
    Vertex { position: [-1.0,-1.0, 1.0], tex_coords: [0.0, 1.0] },
    //BACK FACE
    Vertex { position: [-1.0, 1.0,-1.0], tex_coords: [0.0, 0.0] },
    Vertex { position: [ 1.0, 1.0,-1.0], tex_coords: [1.0, 0.0] },
    Vertex { position: [ 1.0, 1.0, 1.0], tex_coords: [1.0, 1.0] },
    Vertex { position: [-1.0, 1.0, 1.0], tex_coords: [0.0, 1.0] },
  ];

  let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
  let indices = glium::index::IndexBuffer::new(&display, glium::index::PrimitiveType::TrianglesList,
    &[ 0, 1, 2, 0, 2, 3,
       4, 5, 6, 4, 6, 7,
       8, 9,10, 8,10,11,
      12,13,14,12,14,15,
      16,17,18,16,18,19,
      20,21,22,20,22,23u16]).unwrap();

  let program = glium::Program::from_source(&display, include_str!("../assets/rotozoomer-on-cube_150.vert"), include_str!("../assets/rotozoomer-on-cube_150.frag"), None).unwrap();

  'mainloop: loop {
    let mut target = display.draw();
    target.clear_color_and_depth((0.0,0.0,0.0,1.0), 1.0);

    let now = beginning_time.to(time::PreciseTime::now()).num_milliseconds() as i32;

    let perspective = {
      let (width, height) = target.get_dimensions();
      let aspect_ratio = height as f32 / width as f32;

      let fov: f32 = 3.141592 / 4.0;
      let zfar = 1024.0;
      let znear = 0.1;

      let f = 1.0 / (fov / 2.0).tan();

      [
        [f *   aspect_ratio   ,    0.0,              0.0              ,   0.0],
        [         0.0         ,     f ,              0.0              ,   0.0],
        [         0.0         ,    0.0,  (zfar+znear)/(zfar-znear)    ,   1.0],
        [         0.0         ,    0.0, -(2.0*zfar*znear)/(zfar-znear),   0.0],
      ]
    };
    
    let uniforms = uniform! {
      tex: hacklab_texture.sampled()
        .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
        .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
        .wrap_function(glium::uniforms::SamplerWrapFunction::Repeat),

      perspective: perspective,

      view: view_matrix(
        &[(now as f32/1000.0).cos()* 4.0, 1.5, (now as f32/1000.0).sin()* 4.0],
        &[(now as f32/1000.0).cos()*-4.0,-1.5, (now as f32/1000.0).sin()*-4.0],
        &[0.0, 1.0, 0.0]),

      time: now,
    };

    target.draw(&vertex_buffer, &indices, &program, &uniforms,
      &glium::DrawParameters{
        depth: glium::Depth {
          test: glium::draw_parameters::DepthTest::IfLess,
          write: true,
          .. Default::default()
        },
        blend: glium::Blend::alpha_blending(),
        backface_culling: glium::BackfaceCullingMode::CullingDisabled,
        .. Default::default()
    }).unwrap();

    target.finish().unwrap();

    for event in display.poll_events() {
      match event {
        glium::glutin::Event::Closed => break 'mainloop,
        _ => (),
      }
    }
  }
}
