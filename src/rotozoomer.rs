#[macro_use]
extern crate glium;
extern crate image;
extern crate time;

#[derive(Copy, Clone)]
struct Vertex {
  position: [f32; 2],
  tex_coords: [f32; 2],
}

implement_vertex!(Vertex, position, tex_coords);


fn main() {
  use glium::{
    DisplayBuild,
    Surface,
  };

  let beginning_time = time::PreciseTime::now();

  let display = glium::glutin::WindowBuilder::new().with_dimensions(640, 480).build_glium().unwrap();
  
  let hacklab_image = {
    use std::io::Cursor;

    let hacklab_texture = image::load(Cursor::new(&include_bytes!("../assets/hacklab.png")[..]), image::PNG).unwrap().to_rgba();
    let image_dimensions = hacklab_texture.dimensions();
    glium::texture::RawImage2d::from_raw_rgba_reversed(hacklab_texture.into_raw(), image_dimensions)
  };
  let hacklab_texture = glium::texture::SrgbTexture2d::new(&display, hacklab_image).unwrap();

  let shape = vec![
    Vertex { position: [-1.0,-1.0], tex_coords: [0.0, 0.0] },
    Vertex { position: [-1.0, 1.0], tex_coords: [0.0, 1.0] },
    Vertex { position: [ 1.0,-1.0], tex_coords: [1.0, 0.0] },
    Vertex { position: [ 1.0, 1.0], tex_coords: [1.0, 1.0] },
  ];

  let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
  let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

  let program = glium::Program::from_source(&display, include_str!("../assets/rotozoomer_150.vert"), include_str!("../assets/rotozoomer_150.frag"), None).unwrap();

  'mainloop: loop {
    let mut target = display.draw();
    target.clear_color(1.0,1.0,1.0,1.0);
    
    let uniforms = uniform! {
      tex: hacklab_texture.sampled()
        .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
        .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
        .wrap_function(glium::uniforms::SamplerWrapFunction::Repeat),

      resolution: {
        let (width, height) = display.get_framebuffer_dimensions();
        [width as f32, height as f32]
      },

      time: beginning_time.to(time::PreciseTime::now()).num_milliseconds() as i32,
    };

    target.draw(&vertex_buffer, &indices, &program, &uniforms,
      &glium::DrawParameters{
        blend: glium::Blend::alpha_blending(),
        .. Default::default()
    }).unwrap();

    target.finish().unwrap();

    for event in display.poll_events() {
      match event {
        glium::glutin::Event::Closed => break 'mainloop,
        _ => (),
      }
    }
  }
}
