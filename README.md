# Some demo experiments

This is a couple of demo effects written using OpenGL. Written in Rust using the `glium` library.

To run an effect, just run the command `cargo run --bin name`, where `name` is the name of the program. Currently these are:

* `rotozoomer`
* `rotozoomer-on-cube`