#version 150 core

in vec2 position;
in vec2 tex_coords;

uniform mat4 aspect_correction_matrix;

out vec2 v_tex_coords;

void main() {
  v_tex_coords = tex_coords;
  gl_Position = aspect_correction_matrix*vec4(position, 1.0, 1.0);
}
