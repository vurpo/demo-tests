#version 150 core

uniform sampler2D tex;
uniform int time;
uniform vec2 resolution; 

in vec2 v_tex_coords;

out vec4 color;

float BarrelPower = 1.2;
float distortion = 0.7;

const int bloomsamples_x = 2;
const int bloomsamples_y = 2;

vec2 Distort(vec2 p)
{
    float theta  = atan(p.y, p.x);
    float radius = length(p);
    radius = pow(radius, BarrelPower);
    p.x = radius * cos(theta);
    p.y = radius * sin(theta);
    return 0.5 * (p + 1.0);
}

vec2 radialDistortion(vec2 coord) {
  vec2 cc = coord - vec2(0.5);
  float dist = dot(cc, cc) * distortion;
  return coord + cc * (1.0 - dist) * dist;
}

vec4 bloom(vec2 texcoord) {
  vec4 sum = vec4(0);
  vec4 tex_color = texture(tex, texcoord);
  int j;
  int i;

  for (i = -bloomsamples_y; i < bloomsamples_y; i++) {
    for (j = -bloomsamples_x; j < bloomsamples_x; j++) {
      sum += texture(tex, texcoord + vec2(j, i)*0.004) * 0.25;
    }
  }

  if (tex_color.r < 0.3) {
     return sum*sum*0.012 + tex_color;
  } else {
    if (tex_color.r < 0.5) {
      return sum*sum*0.009 + tex_color;
    } else {
      return sum*sum*0.0075 + tex_color;
    }
  }
}

void main() {
  vec2 red_coordinate_shift = vec2(-0.002, 0.0);
  vec2 green_coordinate_shift = vec2(0.0, 0.0);
  vec2 blue_coordinate_shift = vec2(0.002, 0.0);
  
  vec2 distorted_tex_coords = radialDistortion(v_tex_coords);
  vec2 distorted_tex_coords_r = radialDistortion((v_tex_coords+red_coordinate_shift));
  distorted_tex_coords_r.y = round(distorted_tex_coords_r.y*resolution.y)/resolution.y;
  vec2 distorted_tex_coords_g = radialDistortion((v_tex_coords+green_coordinate_shift));
  distorted_tex_coords_g.y = round(distorted_tex_coords_g.y*resolution.y)/resolution.y;
  vec2 distorted_tex_coords_b = radialDistortion((v_tex_coords+blue_coordinate_shift));
  distorted_tex_coords_b.y = round(distorted_tex_coords_b.y*resolution.y)/resolution.y;

  float step = 1.0;
  if (distorted_tex_coords.y < 0.0 || distorted_tex_coords.y > 1.0 || distorted_tex_coords.x < 0.0 || distorted_tex_coords.x > 1.0) {
    step = 0.0;
  }

  color.r = bloom(distorted_tex_coords_r).r;
  color.g = bloom(distorted_tex_coords_g).g;
  color.b = bloom(distorted_tex_coords_b).b;
  color.a = 1.0;
  
  color.rgb *= abs(cos(distorted_tex_coords.y*resolution.y*3.1415));
  
  color *= step;
  
}
