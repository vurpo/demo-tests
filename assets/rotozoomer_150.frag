#version 150 core

uniform sampler2D tex;
uniform int time;
uniform vec2 resolution;

in vec2 v_tex_coords;

out vec4 color;

void main() {
  vec2 coord = v_tex_coords;
  
  coord = vec2(coord.x*(resolution.x/resolution.y), coord.y); 
 
  coord = vec2(
    (coord.x-sin(time/3000.0))*cos(time/2000.0)-(coord.y-cos(time/3000.0))*sin(time/2000.0) + sin(time/3000.0),
    (coord.y-cos(time/3000.0))*cos(time/2000.0)+(coord.x-sin(time/3000.0))*sin(time/2000.0) + cos(time/3000.0));
  
  coord = ((coord-vec2(0.5,0.5))*(sin(time/3000.0)+0.3*cos(time/3000.0*5))*10)+vec2(0.5,0.5);
  
  coord = vec2(coord.x+(time/1000.0+0.5*sin(time/1500.0)), coord.y+time/1000.0);

  float vignette = distance(v_tex_coords, vec2(0.5, 0.5));

  vec4 texcolor = texture(tex, coord);
  
  color = texcolor;
  //color.a = 0.9;
}
