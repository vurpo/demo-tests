#version 150 core

in vec3 position;
in vec2 tex_coords;

uniform mat4 perspective;
uniform mat4 view;

out vec2 v_tex_coords;

void main() {
  
  v_tex_coords = tex_coords;
  gl_Position = perspective*view*vec4(position, 1.0);
}
